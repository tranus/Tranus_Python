#! /usr/bin/env python

import struct
import numpy as np


class L1S_READ(object):

	def __init__(self,file_name,nbSectors,nbZones):
		#nbSectors,nbZones : calculated in L1S_args file

		# path to L1S file
		self.file_name=file_name

		#Thomas parameters 
		self.pro = np.zeros((nbSectors,nbZones))
		self.cospro = np.zeros((nbSectors,nbZones))
		self.precio = np.zeros((nbSectors,nbZones))
		self.coscon = np.zeros((nbSectors,nbZones))
		self.utcon = np.zeros((nbSectors,nbZones))
		self.atrac = np.zeros((nbSectors,nbZones))
		self.ajuste = np.zeros((nbSectors,nbZones))
		self.dem = np.zeros((nbSectors,nbZones))


	def read_L1S(self):
		'''This method is used to read L1S file'''

		offset=0
		list_L1S=[]


		with open(self.file_name, "rb") as f:

			data=f.read()

			header=struct.unpack_from("<3h2i5h3s80s3s80s",data,offset)
			offset=offset+3*2+4+4+5*2+3+80+3+80
			file_major=header[0]
			list_L1S.append(file_major)
			file_minor=header[1]
			list_L1S.append(file_minor)
			file_release=header[2]
			list_L1S.append(file_release)
			ifmt_L1S=header[3] 
			list_L1S.append(ifmt_L1S)
			iter_=header[4]
			list_L1S.append(iter_)
			ian=header[5]
			list_L1S.append(ian)
			mes=header[6]
			list_L1S.append(mes)
			idia=header[7]
			list_L1S.append(idia)
			ihr=header[8]
			list_L1S.append(ihr)
			mins=header[9]
			list_L1S.append(mins)
			area=header[10]
			list_L1S.append(area)
			estudio=header[11]
			list_L1S.append(estudio)
			pol=header[12]
			list_L1S.append(pol)
			nombre=header[13]
			list_L1S.append(nombre)

			num_policy=struct.unpack_from("<2i",data,offset)
			offset=offset+2*4
			npol=num_policy[0]
			list_L1S.append(npol)
			npol_neg=num_policy[1]
			list_L1S.append(npol_neg)

			for i in range(npol):

				info_policy=struct.unpack_from("<2ib5s32s",data,offset)
				offset=offset+2*4+1+5+32
				index_i=info_policy[0]
				list_L1S.append(index_i)
				i_prev_pol=info_policy[1]
				list_L1S.append(i_prev_pol)
				prev_pol_type=info_policy[2]
				list_L1S.append(prev_pol_type)
				nom_pol=info_policy[3] 
				list_L1S.append(nom_pol)
				desc_pol=info_policy[4]
				list_L1S.append(desc_pol)

			npol_ipol_tuple=struct.unpack_from("<2i",data,offset)
			offset=offset+2*4
			npol=npol_ipol_tuple[0] 
			list_L1S.append(npol)
			ipol=npol_ipol_tuple[1] 
			list_L1S.append(ipol)

			sector_1=struct.unpack_from("<3i",data,offset) 
			offset=offset+3*4
			ns=sector_1[0] 
			list_L1S.append(ns)
			ns_neg=sector_1[1] 
			list_L1S.append(ns_neg)
			nflu=sector_1[2] 
			list_L1S.append(nflu)

			for i in range(ns):

				sector_2=struct.unpack_from("<2i32s?5f2i",data,offset) 
				offset=offset+2*4+32+1+5*4+2*4
				index_i=sector_2[0] 
				list_L1S.append(index_i)
				numsec=sector_2[1] 
				list_L1S.append(numsec)
				nomsec=sector_2[2]
				list_L1S.append(nomsec)
				lflu=sector_2[3] 
				list_L1S.append(lflu)
				beta_1=sector_2[4] 
				list_L1S.append(beta_1) 
				beta_2=sector_2[5]
				list_L1S.append(beta_2)
				gama_1=sector_2[6] 
				list_L1S.append(gama_1)
				gama_2=sector_2[7] 
				list_L1S.append(gama_2)
				min_price_to_cost_ratio=sector_2[8] 
				list_L1S.append(min_price_to_cost_ratio)
				sector_type=sector_2[9] 
				list_L1S.append(sector_type)
				target_sector=sector_2[10] 
				list_L1S.append(target_sector)

			ns_tuple=struct.unpack_from("<i",data,offset)
			offset=offset+4
			ns=ns_tuple[0]
			list_L1S.append(ns)

			num_sect=struct.unpack_from("<2i",data,offset) 
			offset=offset+2*4 
			ns=num_sect[0]
			list_L1S.append(ns)
			ns_neg=num_sect[1]
			list_L1S.append(ns_neg)

			for i in range(ns):

				index_i_tuple=struct.unpack_from("<i",data,offset)
				offset=offset+4
				index_i=index_i_tuple[0]
				list_L1S.append(index_i)

				for j in range(ns):

					demand_functions=struct.unpack_from("<i12fi",data,offset)
					offset=offset+4+12*4+4
					index_j=demand_functions[0]
					list_L1S.append(index_j)
					demin=demand_functions[1] 
					list_L1S.append(demin) 
					demax=demand_functions[2] 
					list_L1S.append(demax) 
					delas=demand_functions[3] 
					list_L1S.append(delas)
					selas=demand_functions[4] 
					list_L1S.append(selas) 
					suslgsc=demand_functions[5] 
					list_L1S.append(suslgsc) 
					xalfa_1=demand_functions[6] 
					list_L1S.append(xalfa_1) 
					xalfa_2=demand_functions[7] 
					list_L1S.append(xalfa_2) 
					xalfapro=demand_functions[8]
					list_L1S.append(xalfapro) 
					xalfapre=demand_functions[9] 
					list_L1S.append(xalfapre) 
					xalfacap=demand_functions[10]
					list_L1S.append(xalfacap) 
					alfa_1=demand_functions[11]
					list_L1S.append(alfa_1)
					alfa_2=demand_functions[12]
					list_L1S.append(alfa_2) 
					mxsust=demand_functions[13] 
					list_L1S.append(mxsust) 

					for k in range(mxsust):

						nsust_tuple=struct.unpack_from("<i",data,offset) 
						offset=offset+4
						nsust=nsust_tuple[0] 
						list_L1S.append(nsust)
			
			ns_tuple=struct.unpack_from("<i",data,offset) 
			offset=offset+4
			ns=ns_tuple[0] 
			list_L1S.append(ns)

			param_nzn=struct.unpack_from("<4i",data,offset)
			offset=offset+4*4
			nzn=param_nzn[0] 
			list_L1S.append(nzn)
			nzn_neg=param_nzn[1]
			list_L1S.append(nzn_neg)
			nz_1=param_nzn[2] 
			list_L1S.append(nz_1)
			nz_2=param_nzn[3] 
			list_L1S.append(nz_2)

			for i in range(nzn):

				param_numzon=struct.unpack_from("<2i32s2i",data,offset)
				offset=offset+2*4+32+2*4
				index_i=param_numzon[0]
				list_L1S.append(index_i)
				numzon=param_numzon[1] 
				list_L1S.append(numzon)
				nomzon=param_numzon[2] 
				list_L1S.append(nomzon)
				jer_1=param_numzon[3] 
				list_L1S.append(jer_1)
				jer_2=param_numzon[4] 
				list_L1S.append(jer_2)

			nzn_tuple=struct.unpack_from("<i",data,offset)
			offset=offset+4
			nzn=nzn_tuple[0] 
			list_L1S.append(nzn)

			nzn_ns=struct.unpack_from("<2i",data,offset)
			offset=offset+2*4
			nzn=nzn_ns[0]
			list_L1S.append(nzn)
			ns=nzn_ns[1] 
			list_L1S.append(ns)

			for i in range (nzn):

				numzon_tuple=struct.unpack_from("<2i", data, offset) 
				offset=offset+2*4
				index_i=numzon_tuple[0] 
				list_L1S.append(index_i)
				numzon=numzon_tuple[1] 
				list_L1S.append(numzon)

				for n in range(ns):

					fmt="<idf2dfd3fd4fd3f"
					len_fmt=4+8+4+2*8+4+8+3*4+8+4*4+8+3*4
					param_l1s=struct.unpack_from(fmt,data,offset) 
					offset=offset+len_fmt
					index_n=param_l1s[0]
					list_L1S.append(index_n)
					xpro=param_l1s[1] 
					list_L1S.append(xpro)
					probase=param_l1s[2] 
					list_L1S.append(probase)
					pro=param_l1s[3] 
					self.pro[n,i]=pro
					list_L1S.append(pro)
					cospro=param_l1s[4] 
					self.cospro[n,i]=cospro
					list_L1S.append(cospro)
					prebase=param_l1s[5] 
					list_L1S.append(prebase)
					precio=param_l1s[6] 
					self.precio[n,i]=precio
					list_L1S.append(precio)
					xdem=param_l1s[7] 
					list_L1S.append(xdem)
					dem_xdem=param_l1s[8] 
					self.dem[n,i]=dem_xdem
					list_L1S.append(dem_xdem)
					coscon=param_l1s[9] 
					self.coscon[n,i]=coscon
					list_L1S.append(coscon)
					utcon=param_l1s[10] 
					self.utcon[n,i]=utcon
					list_L1S.append(utcon)
					rmin=param_l1s[11] 
					list_L1S.append(rmin)
					rmax=param_l1s[12]
					list_L1S.append(rmax)
					atrac=param_l1s[13]
					self.atrac[n,i]=atrac
					list_L1S.append(atrac)
					valag=param_l1s[14] 
					list_L1S.append(valag)
					ajuste=param_l1s[15] 
					self.ajuste[n,i]=ajuste
					list_L1S.append(ajuste)
					atrain=param_l1s[16] 
					list_L1S.append(atrain)
					stock=param_l1s[17] 
					list_L1S.append(stock)
					unstock=param_l1s[18]
					list_L1S.append(unstock)

		
		'''return list of Thomas parameters'''
		return [self.pro,self.cospro,self.precio,self.coscon,self.utcon,self.atrac,self.ajuste,self.dem]


	

